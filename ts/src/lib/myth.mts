export class Myth {

    #_number: number;

    constructor(number: number) {
        this.#_number = number;
    }

    get number() {
        return this.#_number;
    }

}
