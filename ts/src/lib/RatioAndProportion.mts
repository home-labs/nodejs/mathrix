import { Myth } from "./myth.mjs";


export class RatioAndProportion extends Myth {

    #proportionalParts: number[];

    constructor(number: number, proportionalParts: number[]) {

        if (typeof number !== 'number' || number === 0) {
            throw new Error('The argument number of constructor must be a non-zero value.');
        }

        super(number);

        this.#proportionalParts = proportionalParts;
    }

    valueFromProportionalPart(proportionalPart: number) {

        const otherProportionalParts = [...this.#proportionalParts];

        const indexOfProportionalPart = otherProportionalParts.indexOf(proportionalPart);

        otherProportionalParts.splice(indexOfProportionalPart, 1);

        return proportionalPart * this
            .#factorOfProportionality(otherProportionalParts.concat(proportionalPart));
    }

    valueFromRatio(ratio: number) {
        return this.number * ratio;
    }

    ratio2(value: number) {
        return value / this.number;
    }

    #factorOfProportionality(proportionalParts: number[] = []) {

        let proportionalPartsSum = proportionalParts.reduce(
            (accumulated, value) => accumulated + value,
            0
        );

        if (!proportionalPartsSum) {
            proportionalPartsSum = this.number;
        }

        return this.number / proportionalPartsSum;
    }

}
