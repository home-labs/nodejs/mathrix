import { Myth } from "./myth.mjs";


export class MythNumber extends Myth {

    isOdd() {
        return (this.number % 2) !== 0;
    }

    static decimalPlacesRound(value: number, decimalPlacesSize = 0) {

        let zerosCount = 0;

        let zeros = '';

        let parts: string[];

        let integerPart: number;

        let remnantDecimalPartDigits: number;

        let significantDecimalPart: number;

        let oldDecimalFirstDigit: number;

        let decimalFirstDigit: number;

        let decimalPart: string;

        let significantDecimalPartAsString: string;

        let result: string;

        if (!decimalPlacesSize) {
            return Math.round(value);
        }

        parts = `${value}`.split('.');

        if (parts.length) {

            integerPart = parseInt(parts[0], 10);
            decimalPart = parts[1];

            if (decimalPlacesSize < decimalPart.length) {

                remnantDecimalPartDigits = parseInt(
                    decimalPart.slice(decimalPlacesSize + 1, decimalPart.length) || '0',
                    10
                );

                significantDecimalPartAsString = decimalPart.slice(0, decimalPlacesSize);
                oldDecimalFirstDigit = parseInt(significantDecimalPartAsString[0], 10);
                significantDecimalPart = parseInt(significantDecimalPartAsString, 10);

                if (parseInt(decimalPart[decimalPlacesSize], 10) > 5 ||
                    // according IBGE resolution number 886 of 1966
                    (parseInt(decimalPart[decimalPlacesSize], 10) === 5 &&
                        (remnantDecimalPartDigits > 0 ||
                            new MythNumber(
                                parseInt(decimalPart[decimalPlacesSize - 1], 10)
                            ).isOdd()
                        )
                    )
                ) {
                    significantDecimalPart += 1;

                    decimalFirstDigit = parseInt(`${significantDecimalPart}`[0], 10);

                    if (significantDecimalPartAsString.length >
                        `${significantDecimalPart}`.length
                    ) {

                        zerosCount = significantDecimalPartAsString.length -
                            `${significantDecimalPart}`.length;

                        zeros = significantDecimalPartAsString.slice(0, zerosCount);
                    }

                    if (!zerosCount && decimalFirstDigit < oldDecimalFirstDigit) {
                        integerPart += 1;
                    }
                }

                result = `${integerPart}`;

                if (zeros.length) {
                    result += `.${zeros}${significantDecimalPart}`;
                } else {
                    result += `.${significantDecimalPart}`;
                }

                return parseFloat(result);
            }
        }

        return value;
    }

}
