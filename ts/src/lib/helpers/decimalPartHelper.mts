export class DecimalPartHelper {

    static denominator4(value: number, intergerPartDigistCount: number) {

        const digits = Math.abs(value).toString().split('');

        const zeros = digits.slice(0, digits.length - intergerPartDigistCount).map(
            digit => '0'
        ).join('');

        return parseInt(`1${zeros}`);
    }

}
