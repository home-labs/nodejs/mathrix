import { Angles } from "./index.mjs";


export class TrigonometryHelper {

    /**
     *
     * @param angle must be in radians
     */
    static calculateAdjacentCathetus(angle: number, hypotenuse: number) {
        return Math.cos(angle) * hypotenuse;
    }

    /**
     *
     * @param angle must be in radians
     */
    static calculateOppositeCathetus(angle: number, hypotenuse: number) {
        return Math.sin(angle) * hypotenuse;
    }

    /**
     *
     * @param angle must be in radians
     */
    static calculateArchLength(angle: number, radius: number) {
        return (2 * Math.PI * radius * angle) / Angles.DegreesHelper.toRadians(360);
    }

    static calculateCircumferenceLength(radius: number) {
        return 2 * Math.PI * radius;
    }

}
