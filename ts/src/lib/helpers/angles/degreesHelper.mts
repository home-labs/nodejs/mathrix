export class DegreesHelper {

    static toRadians(angle: number) {
        /**
         *
         * Em ES, toda função trigonométrica trabalha com ângulo em
         * radianos.
         * o ângulo em radianos, por convenção, equivale ao arco da
         * circunferência cujo o valor unitário é igual ao raio desta
         * circunferência.
         */
        return angle * (Math.PI / 180);
    }

}
