import '../lib/api.mjs';


/**
 *
 * sets doesn't have repeated elements, the object treats these cases
 */
const set1 = new Set([1, 1, 2, 3, 4, 5]);
const set2 = new Set([5, 6, 7]);
const set3 = new Set([4, 5, 6]);

console.log(set1)
console.log(set2)
console.log(set3)
console.log('difference of set 1 to set 2 and 3: ', set1.difference(set2, set3)) //=> [1, 2, 3]
