/// <reference path="./typings.mts"/>

export { };


/**
 *
 * @returns the the recursive difference of main set from the others passed by arbitrary number os parameters
 */
Set.prototype.difference = function <T>(...sets: Set<T>[]): Set<T> {

    let mainSet = Array.from(this);

    let currentDifference: T[] = [];

    sets.forEach(
        set => {

            for (let element of mainSet) {
                if (!set.has(element)) {
                    currentDifference.push(element)
                }
            }

            mainSet = currentDifference;
            currentDifference = [];
    });

    return new Set(mainSet);
};


/**
 *
 * @returns the union of non-repeatable elements
 */
Set.prototype.union = function <T>(...sets: Set<T>[]): Set<T> {

    /**
     *
     * Por causa da instrução "this", não se pode usar uma arrow function
     */
    let union: Set<T> = new Set(this);

    sets.forEach(
        (set: Set<T>) => {
            union = new Set([...union, ...set]);
        }
    );

    return union;
};
