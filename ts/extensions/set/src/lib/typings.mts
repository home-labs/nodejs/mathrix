export {};

declare global {

    interface Set<T> {

        difference(...sets: Set<T>[]): Set<T>;

        union(...sets: Set<T>[]): Set<T>;

    }

}
