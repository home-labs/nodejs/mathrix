/// <reference path="./typings.mts"/>
/**
 *
 * @returns the the recursive difference of main set from the others passed by arbitrary number os parameters
 */
Set.prototype.difference = function (...sets) {
    let mainSet = Array.from(this);
    let currentDifference = [];
    sets.forEach(set => {
        for (let element of mainSet) {
            if (!set.has(element)) {
                currentDifference.push(element);
            }
        }
        mainSet = currentDifference;
        currentDifference = [];
    });
    return new Set(mainSet);
};
/**
 *
 * @returns the union of non-repeatable elements
 */
Set.prototype.union = function (...sets) {
    /**
     *
     * Por causa da instrução "this", não se pode usar uma arrow function
     */
    let union = new Set(this);
    sets.forEach((set) => {
        union = new Set([...union, ...set]);
    });
    return union;
};
export {};
//# sourceMappingURL=api.mjs.map