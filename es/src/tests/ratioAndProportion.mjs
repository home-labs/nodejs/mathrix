import { RatioAndProportion } from '../index.mjs';
const proportionalPart = 2;
const otherProportionalParts = [1, 1];
const proportionalParts = [proportionalPart].concat(otherProportionalParts);
let rationAndProportion = new RatioAndProportion(200, proportionalParts);
console.log(`\nThe ratio from ${rationAndProportion.number} to ${proportionalPart} is ${rationAndProportion.ratio2(proportionalPart)}.`); //=> 0.01
console.log(`\nThe value from ratio ${rationAndProportion.ratio2(proportionalPart)} of the value ${rationAndProportion.number} is ${rationAndProportion.valueFromRatio(rationAndProportion.ratio2(proportionalPart))}.`); //=> 0.01
console.log(`\n${proportionalPart} parts of ${rationAndProportion.number} in the proportion ${proportionalParts.join(':')} is ${rationAndProportion.valueFromProportionalPart(proportionalPart)}.`);
//# sourceMappingURL=ratioAndProportion.mjs.map