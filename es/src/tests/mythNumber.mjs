import { MythNumber } from '../index.mjs';
let decimalPlacesSize = 2;
let totalMythNumber = new MythNumber(4);
console.log(`\n${totalMythNumber.number} is odd?`, totalMythNumber.isOdd());
totalMythNumber = new MythNumber(5.7654);
console.log(`\n${totalMythNumber.number} rounded to ${decimalPlacesSize} decimal places:`, MythNumber.decimalPlacesRound(totalMythNumber.number, 2));
//# sourceMappingURL=mythNumber.mjs.map