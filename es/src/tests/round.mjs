import { MythNumber } from '../index.mjs';
let decimalPlacesSize = 2;
let floatNumber = 5.7654;
// let floatNumber = 5.4644;
let mythNumber = new MythNumber(floatNumber);
console.log(`\n${mythNumber.number} rounded to ${decimalPlacesSize} decimal place(s):`, MythNumber.decimalPlacesRound(mythNumber.number, decimalPlacesSize));
//# sourceMappingURL=round.mjs.map