export class DecimalPlaceHelper {
    static denominator4(value, wholePartSize) {
        const totalDigits = Math.abs(value).toString().split('');
        const zeros = totalDigits.slice(0, totalDigits.length - wholePartSize).map(digit => '0').join('');
        return parseInt(`1${zeros}`);
    }
}
//# sourceMappingURL=decimalPlaceHelper.mjs.map