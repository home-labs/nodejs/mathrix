export class PreviewHelper {
    /**
     *
     * @warn to positive value of angle, the rotatation will be clockwise, otherwise will be anticlockwise
     */
    static mountRotateTransformString(degrees, cx, cy) {
        return `rotate(${degrees}, ${cx}, ${cy})`;
    }
    /**
     *
     * @description
     * It works as anticlockwise, like that in cartesian graph (x, y) = 0 - 90;
     * (-x, y) = 90,... - 180; (-x, -y) = 180,... 270; (x, -y) = 270,... 360.
    */
    static resolveAngle4Rotate(degrees) {
        /**
         *
         * Original
         *     90
         * 180    0
         *    270
         *
         * Treated
         *
         *     90
         * 180    0
         *    -90
        */
        if (degrees) {
            if (degrees < 0) {
                // clockwise
                return Math.abs(degrees);
            }
            else if (degrees > 0) {
                return 360 - degrees;
            }
        }
        return 0;
    }
}
//# sourceMappingURL=previewHelper.mjs.map