import { Angles } from "./index.mjs";
export class TrigonometryHelper {
    /**
     *
     * @param angle must be in radians
     */
    static calculateAdjacentCathetus(angle, hypotenuse) {
        return Math.cos(angle) * hypotenuse;
    }
    /**
     *
     * @param angle must be in radians
     */
    static calculateOppositeCathetus(angle, hypotenuse) {
        return Math.sin(angle) * hypotenuse;
    }
    /**
     *
     * @param angle must be in radians
     */
    static calculateArchLength(angle, radius) {
        return (2 * Math.PI * radius * angle) / Angles.DegreesHelper.toRadians(360);
    }
    static calculateCircumferenceLength(radius) {
        return 2 * Math.PI * radius;
    }
}
//# sourceMappingURL=trigonometryHelper.mjs.map